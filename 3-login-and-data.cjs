const fs = require("fs");
const path = require("path");

// Q1. Create 2 files simultaneously (without chaining).
// Wait for 2 seconds and starts deleting them one after another. (in order)
// (Finish deleting all the files no matter what happens with the creation code. Ensure that this is tested well)

function createFile(noOfFiles) {
  for (let filename = 1; filename <= noOfFiles; filename++) {
    fs.writeFile(`file${filename}.json`, `problem 1`, (err) => {
      if (err) {
        console.error(`Error Occur : ${err}`);
      } else {
        console.log(`File${filename} created`);
      }
    });
  }
}

function createDeletefiles(noOfFiles,secondsToDelete) {
  return new Promise((resolve, reject) => {
    if (noOfFiles) {
      resolve(createFile(noOfFiles));
    } else {
      reject(`Error Occur : ${err}`);
    }
  })
    .then(() => {
      setTimeout(() => {
        for (let filename = 1; filename <= noOfFiles; filename++) {
          fs.unlink(`file${filename}.json`, (err) => {
            if (err) {
              console.error(`Error Occur : ${err}`);
            } else {
              console.log(`File${filename} deleted`);
            }
          });
        }
      }, secondsToDelete*1000);
    })
    .catch((err) => console.error(`Error Occur : ${err}`));
}

createDeletefiles(2,2);

// Q2. Create a new file with lipsum data (you can google and get this).
// Do File Read and write data to another file
// Delete the original file
// Using promise chaining

function lipsumReadWriteDelete(filepath) {
  return new Promise((resolve, reject) => {
    fs.readFile(filepath, "utf-8", (err, data) => {
      if (err) {
        reject(`Error Occur : ${err}`);
      } else {
        console.log("File reading completed");
        resolve(data);
      }
    });
  })
    .then((data) => {
      fs.writeFile("fileForWrite.txt", data, (err) => {
        if (err) {
          console.error(`Error Occur : ${err}`);
        } else {
          console.log("File writing completed");
        }
      });
    })
    .then(() => {
      fs.unlink(filepath, (err) => {
        if (err) {
          console.error(`Error Occur : ${err}`);
        } else {
          console.log("Original file deleted");
        }
      });
    })
    .catch((err) => console.error(`Error Occur : ${err}`));
}
lipsumReadWriteDelete("lipsum.txt");

// function login(user, val) {
//   if (val % 2 === 0) {
//     return Promise.resolve(user);
//   } else {
//     return Promise.reject(new Error("User not found"));
//   }
// }

// function getData() {
//   return Promise.resolve([
//     {
//       id: 1,
//       name: "Test",
//     },
//     {
//       id: 2,
//       name: "Test 2",
//     },
//   ]);
// }

// function logData(user, activity) {
//   // use promises and fs to save activity in some file
// }

/*
Q3.
Use appropriate methods to 
A. login with value 3 and call getData once login is successful
B. Write logic to logData after each activity for each user. Following are the list of activities
    "Login Success"
    "Login Failure"
    "GetData Success"
    "GetData Failure"

    Call log data function after each activity to log that activity in the file.
    All logged activity must also include the timestamp for that activity.
    
    You may use any data you want as the `user` object as long as it represents a valid user (eg: For you, for your friend, etc)
    All calls must be chained unless mentioned otherwise.
    
*/
